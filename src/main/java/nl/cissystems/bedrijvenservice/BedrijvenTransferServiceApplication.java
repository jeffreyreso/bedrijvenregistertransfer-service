package nl.cissystems.bedrijvenservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BedrijvenTransferServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BedrijvenTransferServiceApplication.class, args);
    }

}
