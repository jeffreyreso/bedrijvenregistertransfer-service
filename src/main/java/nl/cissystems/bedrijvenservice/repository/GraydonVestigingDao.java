package nl.cissystems.bedrijvenservice.repository;

import org.springframework.data.repository.CrudRepository;

import nl.cissystems.bedrijvenservice.data.GraydonVestiging;
import nl.cissystems.bedrijvenservice.data.GraydonVestigingKey;

public interface GraydonVestigingDao extends CrudRepository<GraydonVestiging, GraydonVestigingKey>, GraydonVestigingSearchDao {

}

