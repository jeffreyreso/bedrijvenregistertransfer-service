package nl.cissystems.bedrijvenservice.repository;


import nl.cissystems.bedrijvenservice.data.GraydonVestiging;

import java.util.List;

public interface GraydonVestigingSearchDao {

    List<GraydonVestiging> findVestigingenByBedrijfsIds(List<Long> ids);
}