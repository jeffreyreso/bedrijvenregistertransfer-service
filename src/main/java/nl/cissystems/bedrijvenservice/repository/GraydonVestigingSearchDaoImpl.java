package nl.cissystems.bedrijvenservice.repository;


import nl.cissystems.bedrijvenservice.data.GraydonVestiging;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import java.util.List;

public class GraydonVestigingSearchDaoImpl implements GraydonVestigingSearchDao {

    @Autowired
    EntityManager entityManager;

    @Override
    public List<GraydonVestiging> findVestigingenByBedrijfsIds(List<Long> ids) {
        EntityGraph graph = entityManager.getEntityGraph("graph.GraydonVestiging.adressen");

        return entityManager.createQuery(
            "select v " +
                "from GraydonVestiging v " +
                "where v.bedrijfsId in :ids", GraydonVestiging.class)
            .setParameter("ids", ids)
            .setHint("javax.persistence.loadgraph", graph)
            .getResultList();
    }
}
