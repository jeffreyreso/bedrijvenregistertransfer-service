package nl.cissystems.bedrijvenservice.config;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DatabaseConfig {
    @Bean
    public PhysicalNamingStrategy physicalNamingStrategy() {
        return new SpringPhysicalNamingStrategy() {
            @Override
            public Identifier toPhysicalCatalogName(Identifier name, JdbcEnvironment jdbcEnvironment) {
                return apply(name, jdbcEnvironment);
            }

            @Override
            public Identifier toPhysicalSchemaName(Identifier name, JdbcEnvironment jdbcEnvironment) {
                return apply(name, jdbcEnvironment);
            }

            @Override
            public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment jdbcEnvironment) {
                return apply(name, jdbcEnvironment);
            }

            @Override
            public Identifier toPhysicalSequenceName(Identifier name, JdbcEnvironment jdbcEnvironment) {
                return apply(name, jdbcEnvironment);
            }

            @Override
            public Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment jdbcEnvironment) {
                return apply(name, jdbcEnvironment);
            }

            private Identifier apply(Identifier name, JdbcEnvironment jdbcEnvironment) {
                if (name == null) {
                    return null;
                }
                return getIdentifier(name.getText().toLowerCase(), name.isQuoted(), jdbcEnvironment);
            }
        };
    }
}
