package nl.cissystems.bedrijvenservice.controller;

import nl.cissystems.bedrijvenservice.event.BedrijfSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public class BedrijvenController {

    @Autowired
    BedrijfSender sender;

    @GetMapping("/bedrijf/update/{id}")
    public void updateBedrijf(@PathVariable String id) {
        sender.send(id);
    }

    @GetMapping("/bedrijf/loadids")
    public String getBedrijf() {
        try {
            loadBedrijfsIds();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "ok";
    }

    public void loadBedrijfsIds() throws IOException {
        String path = "C:\\Users\\jeffr\\export_id_t.json";
        FileInputStream inputStream = null;
        Scanner sc = null;
        try {
            inputStream = new FileInputStream(path);
            sc = new Scanner(inputStream, "UTF-8");
            int counter = 0;
            int maxSize = 400_000;
            int filecounter = 1;
            while (sc.hasNextLine()) {
                if (counter % 10000 == 0) {
                    System.out.println(LocalDateTime.now() + " " + counter);
                }
                String line = sc.nextLine();
                if (!line.contains("bedrijfsid")) {
                    continue;
                }
                Pattern p = Pattern.compile("([0-9]+)");
                Matcher m = p.matcher(line);
                if (m.find()) {
                    //System.out.println(m.group(0));
                    sender.send(m.group(0));
                    counter++;
                }
            }
            // note that Scanner suppresses exceptions
            if (sc.ioException() != null) {
                throw sc.ioException();
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if (sc != null) {
                sc.close();
            }
        }
    }

}


