package nl.cissystems.bedrijvenservice.data;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@NamedEntityGraph(
    name = "graph.GraydonVestiging.adressen",
    attributeNodes = @NamedAttributeNode("adressen"))
public class GraydonVestiging {

    @Id @EmbeddedId @JsonUnwrapped
    private GraydonVestigingKey key;

    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_Sequence")
    @SequenceGenerator(name = "id_Sequence", sequenceName = "ID_SEQ")
    private Long bedrijfsId;
    private Long kvkNummer;
    private String bedrijfsnaam;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name="bedrijfsnummer"),
        @JoinColumn(name="vestigingsnummer")
    })
    private List<GraydonVestigingAdres> adressen;

}
