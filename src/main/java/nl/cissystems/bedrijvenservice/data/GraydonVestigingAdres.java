package nl.cissystems.bedrijvenservice.data;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
public class GraydonVestigingAdres {
    @Id
    private long id;

    private String huisnummer;
    private String huisnummerToevoeging;
    private String postcodeNumeriek;
    private String postcodeAlfanumeriek;
    private String plaats;
    private String land;
}

