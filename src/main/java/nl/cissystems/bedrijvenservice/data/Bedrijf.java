package nl.cissystems.bedrijvenservice.data;

import lombok.Data;

import javax.persistence.Entity;
import java.util.List;

@Data
public class Bedrijf {

    private long bedrijfsid;
    private String bedrijfsnaam;
    private long vestigingsnummer;
    private long bedrijfsnummer;
    private List<Adres> adressen;

}
