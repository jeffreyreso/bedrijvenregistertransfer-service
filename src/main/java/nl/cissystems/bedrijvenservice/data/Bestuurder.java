package nl.cissystems.bedrijvenservice.data;

import java.util.Date;

public class Bestuurder {
    private long identificatie;
    private String voorletters;
    private String voorvoegsels;
    private String naam;
    private Date geboortedatum;

    public long getIdentificatie() {
        return identificatie;
    }

    public void setIdentificatie(long identificatie) {
        this.identificatie = identificatie;
    }

    public String getVoorletters() {
        return voorletters;
    }

    public void setVoorletters(String voorletters) {
        this.voorletters = voorletters;
    }

    public String getVoorvoegsels() {
        return voorvoegsels;
    }

    public void setVoorvoegsels(String voorvoegsels) {
        this.voorvoegsels = voorvoegsels;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public Date getGeboortedatum() {
        return geboortedatum;
    }

    public void setGeboortedatum(Date geboortedatum) {
        this.geboortedatum = geboortedatum;
    }
}
