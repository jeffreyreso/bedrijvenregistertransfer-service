package nl.cissystems.bedrijvenservice.data;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@NoArgsConstructor
@Embeddable
public class GraydonVestigingKey implements Serializable {
    private long bedrijfsnummer;
    private long vestigingsnummer;

}
