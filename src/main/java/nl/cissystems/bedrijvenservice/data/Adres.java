package nl.cissystems.bedrijvenservice.data;

public class Adres {
    private String huisnummer;
    private String huisnummertoevoeging;
    private String land;
    private String plaats;
    private String postcodealfanumeriek;
    private String postcodenumeriek;

    public String getHuisnummer() {
        return huisnummer;
    }

    public void setHuisnummer(String huisnummer) {
        this.huisnummer = huisnummer;
    }

    public String getHuisnummertoevoeging() {
        return huisnummertoevoeging;
    }

    public void setHuisnummertoevoeging(String huisnummertoevoeging) {
        this.huisnummertoevoeging = huisnummertoevoeging;
    }

    public String getLand() {
        return land;
    }

    public void setLand(String land) {
        this.land = land;
    }

    public String getPlaats() {
        return plaats;
    }

    public void setPlaats(String plaats) {
        this.plaats = plaats;
    }

    public String getPostcodealfanumeriek() {
        return postcodealfanumeriek;
    }

    public void setPostcodealfanumeriek(String postcodealfanumeriek) {
        this.postcodealfanumeriek = postcodealfanumeriek;
    }

    public String getPostcodenumeriek() {
        return postcodenumeriek;
    }

    public void setPostcodenumeriek(String postcodenumeriek) {
        this.postcodenumeriek = postcodenumeriek;
    }
}
