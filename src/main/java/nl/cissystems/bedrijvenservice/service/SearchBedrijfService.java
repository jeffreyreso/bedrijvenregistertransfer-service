package nl.cissystems.bedrijvenservice.service;

import lombok.extern.slf4j.Slf4j;
import nl.cissystems.bedrijvenservice.data.*;
import nl.cissystems.bedrijvenservice.repository.GraydonVestigingDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class SearchBedrijfService {

    @Autowired
    private GraydonVestigingDao graydonVestigingDao;

    public List<GraydonVestiging> getBedrijven(List<Long> ids){
        List<GraydonVestiging> vestigingen = graydonVestigingDao.findVestigingenByBedrijfsIds(ids);
        return vestigingen;
    }

}
