package nl.cissystems.bedrijvenservice.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.cissystems.bedrijvenservice.data.Adres;
import nl.cissystems.bedrijvenservice.data.Bedrijf;
import nl.cissystems.bedrijvenservice.data.GraydonVestiging;
import nl.cissystems.bedrijvenservice.data.GraydonVestigingAdres;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class BedrijfService {

    private static final String INDEX_NAME = "bedrijf";
    private static final String INDEX_TYPE = "_doc";

    @Autowired
    private RestHighLevelClient client;

    @Autowired
    private SearchBedrijfService searchBedrijfService;

    public void updateBedrijven(List<Long> ids) {
        System.out.println(LocalDateTime.now() + " - Start updating");
        List<GraydonVestiging> vestigingen = searchBedrijfService.getBedrijven(ids);
        System.out.println(LocalDateTime.now() + " - Found vestigingen");
        List<Bedrijf> bedrijven = new ArrayList<>();
        for (GraydonVestiging vestiging : vestigingen) {
            if (vestiging != null && StringUtils.isNotBlank(vestiging.getBedrijfsnaam())) {
                Bedrijf bedrijf = new Bedrijf();
                bedrijf.setBedrijfsid(vestiging.getBedrijfsId());
                bedrijf.setBedrijfsnaam(vestiging.getBedrijfsnaam());
                bedrijf.setBedrijfsnummer(vestiging.getKey().getBedrijfsnummer());
                bedrijf.setVestigingsnummer(vestiging.getKey().getVestigingsnummer());
                List<Adres> adressen = new ArrayList<>();
                for (GraydonVestigingAdres graydonAdres : vestiging.getAdressen()) {
                    Adres adres = new Adres();
                    adres.setHuisnummer(graydonAdres.getHuisnummer());
                    adres.setHuisnummertoevoeging(graydonAdres.getHuisnummerToevoeging());
                    adres.setLand(graydonAdres.getLand());
                    adres.setPostcodenumeriek(graydonAdres.getPostcodeNumeriek());
                    adres.setPostcodealfanumeriek(graydonAdres.getPostcodeAlfanumeriek());
                    adres.setPlaats(graydonAdres.getPlaats());
                    adressen.add(adres);
                }
                bedrijf.setAdressen(adressen);
                bedrijven.add(bedrijf);
            }
        }
        insertBedrijven(bedrijven);

        for (long id : ids) {
            if (!inList(id, vestigingen)) {
                deleteBedrijf(id);
            }
        }
        System.out.println(LocalDateTime.now() + " - End updating");
    }

    private boolean inList(long id, List<GraydonVestiging> vestigingen) {
        for (GraydonVestiging vestiging : vestigingen) {
            if (id == (vestiging.getBedrijfsId()))
                return true;
        }
        return false;
    }

    public void insertBedrijven(List<Bedrijf> bedrijven) {
        BulkRequest request = new BulkRequest();
        ObjectMapper mapper = new ObjectMapper();
        for (Bedrijf bedrijf : bedrijven) {
            IndexRequest indexReq = new IndexRequest(INDEX_NAME);
            indexReq.id(String.valueOf(bedrijf.getBedrijfsid()));
            indexReq.type(INDEX_TYPE);
            try {
                byte[] json = mapper.writeValueAsBytes(bedrijf);
                indexReq.source(json, XContentType.JSON);
            } catch (IOException e) {
                e.printStackTrace();
            }
            request.add(indexReq);
        }
        try {
            BulkResponse bulkResponse = client.bulk(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteBedrijf(long id) {
        DeleteRequest request = new DeleteRequest(INDEX_NAME, INDEX_TYPE, String.valueOf(id));
        try {
            client.delete(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
