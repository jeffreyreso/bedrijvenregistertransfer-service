package nl.cissystems.bedrijvenservice.event;

import nl.cissystems.bedrijvenservice.service.BedrijfService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BedrijfReceiver {

    @Autowired
    BedrijfService bedrijfservice;

    List<Long> idList = new ArrayList<>();

    @RabbitListener(queues = "bedrijf")
    public void receive(String id) {
        System.out.println(" [x] Received '" + id + "'");
        idList.add(Long.parseLong(id));
        if (idList.size() >= 1000) {
            System.out.println("Processing bulk...");
            bedrijfservice.updateBedrijven(idList);
            idList.clear();
        }

    }
}
