package nl.cissystems.bedrijvenservice.event;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BedrijfSender {

    @Autowired
    private RabbitTemplate template;

    @Autowired
    private static final String queueName = "bedrijf";

    public void send(String id) {
        this.template.convertAndSend(queueName, id);
        System.out.println(" [x] Sent '" + id + "'");
    }
}
